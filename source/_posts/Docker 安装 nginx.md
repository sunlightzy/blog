---

title: docker 安装 nginx
categories:
  - docker
tags:
  - docker
  - nginx
toc: true
date: 2018-08-05 22:59:00
sidebar:

---

# Docker 安装 nginx

### 方式一：通过  pull 仓库镜像

#### 一、下载镜像

```shell
docker pull nginx
```

#### 二、使用镜像创建容器

```shell
cd ~
mkdir -p ~/nginx/www ~/nginx/logs ~/nginx/conf
#www目录将映射为nginx容器配置的虚拟目录
#logs目录将映射为nginx容器的日志目录
#conf目录里的配置文件将映射为nginx容器的配置文件
#找一份默认的 nginx.conf 配置文件放在 conf 目录下,否则下面启动会报错
docker run -p 80:80 --name web -v $PWD/www:/www -v  $PWD/logs:/wwwlogs -d nginx
docker cp web:/etc/nginx/nginx.conf 
#删除容器后再运行下面的命令
docker run -p 80:80 --name web --link=app1:app1 --link=app2:app2 --link=app3:app3 -v $PWD/www:/www -v $PWD/conf/nginx.conf:/etc/nginx/nginx.conf -v $PWD/conf/servers:/etc/nginx/conf.d -v $PWD/logs:/wwwlogs -d nginx
#此时打开浏览器访问宿主机的 IP 就可看到 nginx 的界面了，安装启动成功
#-d:让容器在后台运行。
#-P:将容器内部使用的网络端口映射到我们使用的主机上。

#使用命令进入交互式终端
docker exec -it mynginx /bin/bash

#查看 IP
ifconfig
#发现找不到指令，需要安装 net-tools 工具依次执行，再执行 ifconfig
apt-get update
apt-get install net-tools

#在宿主机中查询容器的 IP，返回 json 串，里面包含了详细的容器信息，包括 IP ~
#docker inspect [容器名|id]
docker inspect mynginx
```

命令说明：

- -p 80:80：将容器的80端口映射到主机的80端口
- --name  web：将容器命名为web
- -v $PWD/www:/www：将主机中当前目录下的www挂载到容器的/www
- -v $PWD/conf/nginx.conf:/etc/nginx/nginx.conf：将主机中当前目录下的nginx.conf挂载到容器的/etc/nginx/nginx.conf
- -v $PWD/logs:/wwwlogs：将主机中当前目录下的logs挂载到容器的/wwwlogs

### 方式二：通过 Dockerfile构建

##### 构建准备工作

```shell
mkdir -p ~/nginx/www ~/nginx/logs ~/nginx/conf
cd ~/nginx
vi Dockerfile
```

##### 在 Dockerfile 中输入如下内容：

```tex
#指定使用那个基础镜像
FROM centos
MAINTAINER ginkgo
LABEL Discription="基于centos的nginx镜像" version="1.0"
WORKDIR /usr/local/src
RUN yum install -y wget
RUN wget http://nginx.org/download/nginx-1.8.0.tar.gz
RUN tar -zxvf nginx-1.8.0.tar.gz
WORKDIR nginx-1.8.0
#安装nginx所依赖的包
RUN yum -y install gcc-c++
RUN yum -y install pcre pcre-devel
RUN yum -y install zlib zlib-devel
RUN yum -y install openssl openssl-devel libssl-dev
RUN ./configure
RUN make
RUN make install
EXPOSE 80
```

##### 通过Dockerfile 构建一个镜像

```shell
# -t 镜像名 , "." 是Dockerfile 所在的目录，可以使用绝对路径
docker build -t ginkgo/nginx .
#查看镜像
docker images
```

##### 构建|运行容器

```shell
#找一份默认的 nginx.conf 配置文件放在 ~/nginx/conf 目录下,否则下面启动会报错
docker run -p 80:80 --name mynginx -v $PWD/www:/www -v $PWD/conf/nginx.conf:/etc/nginx/nginx.conf -v $PWD/logs:/wwwlogs -d nginx
#此时打开浏览器访问宿主机的 IP 就可看到 nginx 的界面了，安装启动成功
#-d:让容器在后台运行。
#-P:将容器内部使用的网络端口映射到我们使用的主机上。

#使用命令进入交互式终端
docker exec -it mynginx /bin/bash

#查看 IP
ifconfig
#发现找不到指令，需要安装 net-tools 工具依次执行，再执行 ifconfig
apt-get update
apt-get install net-tools

#在宿主机中查询容器的 IP，返回 json 串，里面包含了详细的容器信息，包括 IP ~
docker inspect [容器名|id]
```

