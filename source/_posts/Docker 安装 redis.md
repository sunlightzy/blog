---

title: docker 安装redis
categories:
  - docker
tags:
  - docker
  - redis
toc: true
date: 2018-08-05 22:59:00
sidebar:

---

# Docker 安装 Redis

##### 下载镜像|创建运行容器

```shell
docker pull redis
#创建|运行容器
docker run -d --name=myredis -p 6379:6379 redis
```

##### 其他容器与 redis 容器通讯

nginx容器需要与redis容器通信的话首先要知道它的ip地址，但是每次都手工获取容器的ip地址显然是一件繁琐的事情，于是我们需要修改容器的启动方式，加--link参数，建立其他容器与redis容器之间的联系。

删除掉之前的容器，现在重新修改 nginx容器的启动方式：

```shell
# 使用 pwd 变量
cd ~/nginx && docker run -it -p 80:80 --name=mynginx --link=myredis:db -v $PWD/www:/www -v $PWD/conf/nginx.conf:/etc/nginx/nginx.conf -v $PWD/logs:/wwwlogs -d nginx

docker run -it -p 80:80 --name=web --link=app1:app1 --link=app2:app2 --link=app3:app3 -v $PWD/www:/www -v $PWD/conf/nginx.conf:/etc/nginx/nginx.conf -v $PWD/logs:/wwwlogs -d nginx
```

这次加入了两个参数：

- **-v /code:/usr/src/app** 表示把宿主机上的/code目录挂载到容器内的/usr/src/app目录，可以通过直接管理宿主机上的挂载目录来管理容器内部的挂载目录。
- **--link=redis:db** 表示把redis容器以db别名与该容器建立关系，在该容器内以db作为主机名表示了redis容器的主机地址。

现在进入到其他容器，通过ping命令确认nginx容器能访问到redis容器：

```shell
$ ping db
PING db (192.168.32.12): 56 data bytes
64 bytes from 192.168.32.12: icmp_seq=0 ttl=64 time=0.463 ms
64 bytes from 192.168.32.12: icmp_seq=1 ttl=64 time=0.086 ms
#ping 命令找不到
$ apt-get update
$ apt-get install inetutils-ping
```

##### Redis 集群搭建

```shell
#创建 redis master 容器
$ docker run -d --name=redis_master -p 6380:6379 redis
#两个  redis slave 容器
$ docker run -d --name=redis_slave_1 -p 6380:6379 --link=redis_master:master redis redis-server --slaveof master 6379
$ docker run -d --name=redis_slave_2 -p 6381:6379 --link=redis_master:master redis redis-server --slaveof master 6379
```

现在写入到Redis主节点的数据都会在从节点上备份一份数据。

![img](https://pic4.zhimg.com/80/v2-b97bbd401b2f169c36a3b660206c1abe_hd.jpg)

为了防止 master 宕机，再由Sentinel集群根据投票选举出slave节点作为新的master。

下面为Sentinel编写Dockerfile，在redis镜像的基础上作改动：

```shell
FROM redis:latest
COPY run-sentinel.sh /run-sentinel.sh
COPY sentinel.conf /etc/sentinel.conf
RUN chmod +x /run-sentinel.sh
ENTRYPOINT ["/run-sentinel.sh"]
```

Sentinel的配置文件：

```
port 26379
dir /tmp
sentinel monitor master redis-master 6379 2
sentinel down-after-milliseconds master 30000
sentinel parallel-syncs master 1
sentinel failover-timeout master 180000
```

run-sentinel.sh：

```
#!/bin/bash
exec redis-server /etc/sentinel.conf --sentinel
```

构建出Sentinel的镜像文件，容器运行的方式类似于redis：

```shell
$ docker run -d --name=sentinel_1 --link=redis_master:redis-master [build_sentinel_image]
$ docker run -d --name=sentinel_2 --link=redis_master:redis-master [build_sentinel_image]
$ docker run -d --name=sentinel_3 --link=redis_master:redis-master [build_sentinel_image]
```

这下Sentinel的容器也搭建起来了，应用的结构图如下：

![](https://pic1.zhimg.com/80/v2-998b4229612939a15a00ec1905698ce4_hd.jpg)