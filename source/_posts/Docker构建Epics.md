---

title: Docker构建Epics
categories:
  - docker
tags:
  - docker
  - epics
toc: true
date: 2018-08-05 22:59:00
sidebar:

---

# Docker构建Epics



## 基于MySQL:5.7.24镜像

```sh
docker pull mysql:5.7.24
docker run -p 3307:3306 --name mysql -e MYSQL_ROOT_PASSWORD=123456 -d mysql:5.7.24
docker exec -it mysql /bin/bash
# mysql 镜像是基于debian发行版Linux系统
apt-get update # 更新源
apt-get install build-essential  
# 这个命令最方便，把所有要安装的全部安装好：build-essential是c语言的开发包，包含了gcc make gdb和libc函数库等很多工具。

# 保证perl gcc g++ c++ 都已安装
which perl 
which gcc
which g++
which c++
# 编译中可能出现缺少 readline.h
apt-get install libreadline-dev
```

## 准备安装

```sh
cd ~
mkdir epics 
cd epics
mkdir extensions # 存放扩展程序
cd /usr/src
wget https://epics.anl.gov/download/base/base-3.15.5.tar.gz
tar -vxzf base-3.15.5.tar.gz
ln -s base-3.15.5.tar.gz base #创建软连接
cd base
./startup/EpicsHostArch # 获取系统架构, 我的是linux-x86_64
pwd # 输出 /home/parallels/epics/base
cd ~
vi .bashrc
# 添加
# export EPICS_HOST_ARCH=linux-x86_64
# export HOST_ARCH=linux-x86_64
# 上面的linux-x86_64根据系统情况设置，具体参考base/configure/CONFIG_SITE
# export EPICS_EXTENSIONS=/home/parallels/epics/extensions
# export EPICS_BASE=/home/parallels/epics/base
source .bashrc # 使环境变量生效
cd ~/epics/base/startup
./EpicsHostArch # 获取系统架构, 我的是linux-x86_64
cd ~/epics/base/configure
vim CONFIG_SITE
# 填写下面几项值 , 下面的值来自于获取系统架构的输出
CROSS_COMPILER_TARGET_ARCHS=linux-x86_64
CROSS_COMPILER_HOST_ARCHS=linux-x86_64
CROSS_COMPILER_RUNTEST_ARCHS=linux-x86_64
```

安装Python3

```sh
apt-get install python3
```

安装pip3

```
apt-get install python3-pip
```

