---

title: Epics编译安装
categories:
  - epics
tags:
  - epics
toc: true
date: 2018-08-05 22:59:00
sidebar:

---

# Epics编译安装

## 我的系统环境

> macOS Mojave parallels desktop虚拟机系统: centos 7 linux-x86_64
>
> Epics版本: [3.15.5](https://epics.anl.gov/download/base/index.php) 下载[链接](https://epics.anl.gov/download/base/base-3.15.5.tar.gz)

### 记一笔:

parallels desktop下载的centos7 默认用户名是parallels 密码是需要设置的。软件没有自动设置。密码必须大于8位；

并且无法进行su命令，提示 Authentication failure。

这个问题产生的原因是由于系统默认是没有激活root用户的，需要我们手工进行操作，在命令行界面下，或者在终端中输入如下命令：

```sh
sudo passwd
Password：你当前的密码
Enter new UNIX password：这个是root的密码
Retype new UNIX password：重复root的密码
```

然后会提示成功的信息。 在说明一点，使用su和sudo是有区别的，使用su切换用户需要输入所切换到的用户的密码，而使用sudo则是当前用户的密码。

## 安装准备

```sh
cd ~
mkdir epics 
cd epics
mkdir extensions # 存放扩展程序
cd .. 
wget https://epics.anl.gov/download/base/base-3.15.5.tar.gz
tar -vxzf base-3.15.5.tar.gz
ln -s base-3.15.5.tar.gz base #创建软连接
cd base
./startup/EpicsHostArch # 获取系统架构, 我的是linux-x86_64
pwd # 输出 /home/parallels/epics/base
cd ~
vi .bashrc
# 添加
# export EPICS_HOST_ARCH=linux-x86_64
# export HOST_ARCH=linux-x86_64
# 上面的linux-x86_64根据系统情况设置，具体参考base/configure/CONFIG_SITE
# export EPICS_EXTENSIONS=/home/parallels/epics/extensions
# export EPICS_BASE=/home/parallels/epics/base
source .bashrc # 使环境变量生效
cd ~/epics/base/startup
./EpicsHostArch # 获取系统架构, 我的是linux-x86_64
cd ~/epics/base/configure
vim CONFIG_SITE
# 填写下面几项值 , 下面的值来自于获取系统架构的输出
CROSS_COMPILER_TARGET_ARCHS=linux-x86_64
CROSS_COMPILER_HOST_ARCHS=linux-x86_64
CROSS_COMPILER_RUNTEST_ARCHS=linux-x86_64

```

**为了确保安装过程顺利**

```sh
# 编译中可能出现缺少 readline.h
sudo yum install readline-static.x86_64
# 确保环境安装了 g++ c++ gcc perl
which perl # 输出 /usr/bin/perl表示安装了perl,其他三个类似
# 由于我的环境没有g++ 和 c++ ,安装一下
sudo yum install gcc gcc-c++
```

Make安装

```sh
cd ~/epics/base # 回到epics base的根目录
make 
# 下面就是漫长的等待...
# 如果没有什么问题就成功了,如果编译报缺失什么文件,安装后再依次执行 
# make distclean 
# make
# 我的make过程很顺利,花了大概1分钟,但是我在云主机上花了30多分钟
ls 
# bin  configure  db  dbd  documentation  html  include  lib  LICENSE  Makefile  README  src  startup  templates
vi ~/.bashrc
# 添加
PATH=$PATH:/home/parallels/epics/base/bin/linux-x86_64
export PATH
# 生效环境变量
source ~/.bashrc
```

创建example软件和IOC环境

```sh
cd ~/epics
mkdir -p iocs/example
cd iocs
cd example
makeBaseApp.pl -t example example
makeBaseApp.pl -i -t example example
ls
# configure  exampleApp  iocBoot  Makefile
make # 等待完成
ls
# bin  configure  db  dbd  exampleApp  include  iocBoot  lib  Makefile
cd iocBoot/iocexample/
ls
# envPaths  Makefile  README  st.cmd
sudo chmod +x ./st.cmd
./st.cmd
epics> dbl
parallels:xxxExample
parallels:compressExample
parallels:calcExample
parallels:calcExample1
parallels:calc1
parallels:calcExample2
parallels:calc2
parallels:calcExample3
parallels:calc3
parallels:aSubExample
parallels:subExample
parallels:aiExample
parallels:aiExample1
parallels:ai1
parallels:aiExample2
parallels:ai2
parallels:aiExample3
parallels:ai3
epics> dbpr parallels:ai1
ASG:                DESC: Analog input No. 1                DISA: 0
DISP: 0             DISV: 1             NAME: parallels:aiExample1
RVAL: 0             SEVR: MAJOR         STAT: LOLO          SVAL: 0
TPRO: 0             VAL: 0
```

开启一个新终端,继续输入

```
camonitor parallels:aiExample
```

mac os 安装问题

1. EXTERN.h 文件缺失:是因为perl的原因, 解决方案 brew install perl-build