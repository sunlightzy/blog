---

title: Greys 使用手册 [安装]
categories:
  - greys
tags:
  - greys
toc: true
date: 2018-07-05 22:59:00
sidebar:

---

# Greys 使用手册 [安装]

Greys支持在线安装和本地安装两种安装方案，安装即可用，推荐使用在线安装。

## 在线安装（推荐）

请复制以下内容，并粘贴到命令行中。

```sh
curl -sLk http://ompc.oss.aliyuncs.com/greys/install.sh|bash
```

命令将会下载的启动脚本文件`greys.sh`到当前目录，你可以放在任何地方或加入到`$PATH`中

## 本地安装

在某些情况下，目标服务器无法访问远程阿里云主机，此时你需要自行下载greys的安装文件。

1. 下载最新版本的Greys

   <http://ompc.oss.aliyuncs.com/greys/release/greys-stable-bin.zip>

2. 解压zip文件后，执行以下命令

   ```sh
   cd greys
   sh ./install-local.sh
   ```

   即完成本地安装。

## 版本管理

- **多版本管理**

  从`1.7.0.1`版本开始，`greys.sh`支持自动更新，在网络允许的情况下会自动监测远程服务器上是否存在可升级的最新版本。

  若网络不可达（网络隔离的环境）则需要进行本地安装。本地安装的greys也一样会纳入到多版本管理识别范围。

- **大版本兼容性问题**

  大版本之间不做任何兼容性保障，比如`1.7.0.0`版本的客户端不保证能访问`1.6.0.0`启动的服务端。

## 常见安装问题

- **下载失败**

  通常这样的原因你需要检查你的网络是否畅通，核对是否能正确访问这个网址<http://ompc.oss.aliyuncs.com/greys/greys.sh>

  ```sh
  downloading...
  download failed!
  ```

- **没有权限**

  安装脚本首先会将greys文件从阿里云服务器上下载到当前执行脚本的目录，所以你必须要拥有当前目录的写权限。

  ```sh
  permission denied, target directory is not writable.
  ```