---
title: Hexo基本使用
date: 2015-04-21 23:20:36
categories:
  - hexo
tags:
  - hexo
toc: true # 是否启用内容索引
sidebar:  # 是否启用sidebar侧边栏，none：不启用
---
官网 [Hexo](https://hexo.io/)| 文档 [documentation](https://hexo.io/docs/) | 社区 [troubleshooting](https://hexo.io/docs/troubleshooting.html) | Git地址 [GitHub](https://github.com/hexojs/hexo/issues).

## 快速入门

### 新建文章

``` bash
$ hexo new "My New Post"
```

详细说明: [Writing](https://hexo.io/docs/writing.html)

### 运行

``` bash
$ hexo server
```

详细说明: [Server](https://hexo.io/docs/server.html)

### 生成静态文件

``` bash
$ hexo generate
```

详细说明: [Generating](https://hexo.io/docs/generating.html)

### 发布到远程站点

``` bash
$ hexo deploy
```

详细说明: [Deployment](https://hexo.io/docs/deployment.html)
