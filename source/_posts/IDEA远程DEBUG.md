---
title: IDEA远程debug
date: 2017-09-21 23:20:36
categories:
  - idea
tags:
  - idea
  - 远程debug
toc: true # 是否启用内容索引
sidebar:  # 是否启用sidebar侧边栏，none：不启用
---



#### 一. 背景：

在测试工作中，为方便发现代码中的逻辑问题，尝试使用远程`debug`模式，在测试过程中走查代码，不仅可以辅助测试减少与开发的沟通成本，更便于了解业务提升测试深度。

#### 二. 配置方式：

1. 调试的配置方式主要为设置JVM的参数，使之工作在debug模式下，常用参数为：

```sh
-Xdebug -Xrunjdwp:transport=dt_socket,address=8012,server=y,suspend=n
```

1. 服务器端配置：

若项目为`web`项目，可在`tomcat`的启动程序如`catalina.sh`中添加如下：

```sh
CATALINA_OPTS="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=28888"
```

若项目为`javaapp`项目，可在项目的`default`文件中添加如下：

```sh
if [-z "$XDEBUG_ADDRESS"]; then
    JAVA_OPTS="${JAVA_OPTS} -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=28888"
fi
```

1. idea配置

打开`idea`中的`run/debug configurations`, 选择`remote`类型，地址配置为服务器地址，端口配置为上述配置参数中的`address`

![image-20181207214858996](https://ws1.sinaimg.cn/large/006tNbRwly1fxyiqk9xyzj312c0u047l.jpg)

1. 重启项目，即可开启远程`debug`模式

#### 三. 参数解释：

1. `JAVA`支持调试功能，并提供了一个简单的调试工具`JDB`，其可支持设置断点及线程级的调试；

2. 各参数解释：

   - `-Xdebug`是通知`JVM`工作在`DEBUG`模式下

   - `-Xrunjdwp`是通知`JVM`使用(`java debug wire protocol`)来运行调试环境。该参数同时了一系列的调试选项：

   - `transport`指定了调试数据的传送方式，`dt_socket`是指用`SOCKET`模式，另有`dt_shmem`指用共享内存方式，其中，`dt_shmem`只适用于`Windows`平台。

   - `server`参数是指是否支持在`server`模式的`VM`中.

   - `onthrow`指明，当产生该类型的`Exception`时，`JVM`就会中断下来，进行调式。该参数可选。

   - `launch`指明，当`JVM`被中断下来时，执行的可执行程序。该参数可选

   - `suspend`指明，是否在调试客户端建立起来后，再执行`JVM`。

   - `onuncaught`(=y或n)指明出现`uncaught exception` 后，是否中断`JVM`的执行.