---
title: JVM 分析工具概述
categories:
  - jvm
tags:
  - jvm
toc: true
date: 2018-07-05 22:59:00
sidebar:
---

# JVM 分析工具概述

## 指令简述

- `JPS` : `java process status` 查看进程
- `Jstat`: 类装载、内存、垃圾收集、jit编译信息
- `Jinfo`: 实时查看和调整虚拟机的各项参数
- `Jmap`: 打印JVM堆内对象情况,查询Java堆和永久代的详细信息, 使用率, 使用大小
- `Jhat` : `jvm heap analysis tool`  `jvm`堆分析工具, 内存占用较高
- `Jstack`: 打印线程堆栈信息
- `Jconsole`: 内存监控
- `visualVm`: 可视化的`java`虚拟机界面

#### JPS(java process status)

```sh
$ jps
# PID | 主函数类名
29057 CanalLauncher
28849 BrokerStartup
31315 Jps
28870 rocketmq-console-ng-1.0.0.jar
488 QuorumPeerMain
30297 RemoteMavenServer

$ jps -m  # 输出主函数传入的参数
29057 CanalLauncher
28849 BrokerStartup -n localhost:9876
28870 rocketmq-console-ng-1.0.0.jar
488 QuorumPeerMain /usr/local/etc/zookeeper/zoo.cfg
30297 RemoteMavenServer

$ jps -q  # 只显示PID
31328
29057
28849
28870
488
30297

$ jps -l  # 输出应用程序主类全类名或启动jar包的完整路径名
29057 com.alibaba.otter.canal.deployer.CanalLauncher
28849 org.apache.rocketmq.broker.BrokerStartup
28870 /Users/ginkgo/myapp/rocketmq-console/target/rocketmq-console-ng-1.0.0.jar
31335 sun.tools.jps.Jps
488 org.apache.zookeeper.server.quorum.QuorumPeerMain
30297 org.jetbrains.idea.maven.server.RemoteMavenServer

$ jps -v  # 列出jvm参数
345 Elasticsearch -Xms1g -Xmx1g -XX:+UseConcMarkSweepGC -XX:CMSInitiatingOccupancyFraction=75 -XX:+UseCMSInitiatingOccupancyOnly -XX:+AlwaysPreTouch -Xss1m -Djava.awt.headless=true -Dfile.encoding=UTF-8 -Djna.nosys=true -XX:-OmitStackTraceInFastThrow -Dio.netty.noUnsafe=true -Dio.netty.noKeySetOptimization=true -Dio.netty.recycler.maxCapacityPerThread=0 -Dlog4j.shutdownHookEnabled=false -Dlog4j2.disable.jmx=true -Djava.io.tmpdir=/var/folders/3v/pls8r6nj2jz09pbtmqxcxy700000gn/T/elasticsearch.FXTflx5J -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=data -XX:ErrorFile=logs/hs_err_pid%p.log -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+PrintTenuringDistribution -XX:+PrintGCApplicationStoppedTime -Xloggc:logs/gc.log -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=32 -XX:GCLogFileSize=64m -Des.path.home=/usr/local/Cellar/elasticsearch/6.4.2/libexec -Des.path.conf=/usr/local/etc/elasticsearch -Des.distribution.flavor=oss -Des.distribution.type=tar

```

详情请参考[地址](https://docs.oracle.com/javase/8/docs/technotes/tools/unix/jps.html)

#### 获取远程服务器 jps 信息

`jps` 支持查看远程服务上的 `jvm` 进程信息。如果需要查看其他机器上的  `jvm` 进程，需要在待查看机器上启动 `jstatd` 服务。

#### 开启 jstatd 服务

启动 `jstatd` 服务，需要有足够的权限。 需要使用 `Java` 的安全策略分配相应的权限。

创建 `jstatd.all.policy` 策略文件。

```sh
grant codebase "file:${java.home}/../lib/tools.jar" {
    permission java.security.AllPermission;
};
```

启动 `jstatd` 服务器

```sh
jstatd -J-Djava.security.policy=jstatd.all.policy -J-Djava.rmi.server.hostname=192.168.31.241

-J 参数是一个公共的参数，如 jps、 jstat 等命令都可以接收这个参数。 由于 jps、 jstat 命令本身也是 Java 应用程序， -J 参数可以为 jps 等命令本身设置 Java 虚拟机参数。
-Djava.security.policy：指定策略文件
-Djava.rmi.server.hostname：指定服务器的ip地址（可忽略）

```

默认情况下， `jstatd` 开启在 `1099` 端口上开启 `RMI` 服务器。

