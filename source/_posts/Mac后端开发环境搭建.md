---
title: Mac后端开发环境搭建
date: 2018-04-14 10:01:56
categories:
  - mac
tags:
  - mac
toc: true # 是否启用内容索引
sidebar:  # 是否启用sidebar侧边栏，none：不启用
---

`作为一个开发人员，选择 Mac 是一个非常好的选择，首先 Mac 是 Unix 的内核，支持 Unix 内核的命令，使用 Mac 能帮助我们熟悉 Unix 的操作命令`

## 1、HomeBrew

### 1.1 简介

`Homebrew是一款Mac OS平台下的软件包管理工具，拥有安装、卸载、更新、查看、搜索等很多实用的功能。简单的一条指令，就可以实现包管理，而不用你关心各种依赖和文件路径的情况，十分方便快捷。`

`简单点说，Homebrew 是以最简单、最灵活的方式来安装苹果公司在 MacOS 中不包含的 UNIX 工具。`

### 1.2 安装与卸载 

#### 安装

打开终端，复制粘贴，大约1分钟左右，下载完成，过程中需要输入密码，其他无需任何操作：

`/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`

#### 卸载

有安装就要有卸载，打开终端，复制粘贴： 

`/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/uninstall)"`

其实只用把上面安装的install换成uninstall就行了。

#### 使用

Homebrew 常用命令，下面以安装 git 为例（使用 brew 安装默认安装软件包的最新版本）

```shell
安装任意软件包：
brew install git
卸载已安装软件包：
brew uninstall git
搜索可用软件包：
brew search git
查看任意软件包信息：
brew info git
更新已安装的软件包：
brew upgrade git
查看所有已安装的软件包：
brew list
更新 Homebrew：
brew update
查看 HomeBrew 版本：
brew -v
HomeBrew 帮助信息：
brew -h
```

使用 `brew -h` 看下官方帮助：

```shell
$ brew -h
Example usage:
  brew search [TEXT|/REGEX/]
  brew info [FORMULA...]
  brew install FORMULA...
  brew update
  brew upgrade [FORMULA...]
  brew uninstall FORMULA...
  brew list [FORMULA...]

Troubleshooting:
  brew config
  brew doctor
  brew install --verbose --debug FORMULA

Contributing:
  brew create [URL [--no-fetch]]
  brew edit [FORMULA...]

Further help:
  brew commands
  brew help [COMMAND]
  man brew
  https://docs.brew.sh
```

#### 友情提示

在`Mac OS X 10.11`系统以后，`/usr/local/`等系统目录下的文件读写是需要系统root权限的，以往的Homebrew安装如果没有指定安装路径，会默认安装在这些需要系统root用户读写权限的目录下，导致有些指令需要添加sudo前缀来执行，比如升级Homebrew需要：`sudo brew update`

**推荐:**   安装Homebrew时对安装路径进行指定，直接安装在不需要系统root用户授权就可以自由读写的目录下

```shell
/usr/bin/ruby <install path> -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

下面是我的 HomeBrew 的版本：

```shell
$ brew -v
Homebrew 1.6.0
Homebrew/homebrew-core (no git repository)
```

默认安装的所有的命令都在`/usr/local/bin`目录下，安装文件都在`/usr/local/Cellar`下，对应的配置文件都在`/usr/local/etc`下

### 1.3 Cakebrew

Cakebrew 是 HomeBrew 的 GUI 版本，提供图形化的方式安装和管理软件包，安装方式：

```shell
brew cask install cakebrew
```

### 1.4  homebrew-cask

homebrew-cask安装常用软件，比在网上下载安装文件安装的优势在于：
（1）节省下载安装包的过程，一行命令即可安装
（2）一些在网上搜不到安装文件的软件也可以通过这种方法安装

```shell
brew tap phinze/homebrew-cask
brew install brew-cask
```

使用方法：将上面的brew换成brew cask即可，如

```shell
brew cask install qq
```

使用 brew cask 安装常用的软件：

brew cask 搜索地址 <https://caskroom.github.io/search>

```text
brew cask install yy
brew cask install qq
brew cask install dash # 帮助文档
brew cask install atom
brew cask install sequel-pro # mysql可视化工具
brew cask install sourcetree # git可视化工具
brew cask install neteasemusic # 网易云音乐
brew cask install android-file-transfer # android 传输工具
brew cask install android-studio
brew cask install intellij-idea
brew cask install visual-studio-code
brew cask install mockplus # 比较不错的画原型工具
brew cask install alfred # 小红帽
brew cask install the-unarchiver # 压缩工具
brew cask install thunder # 迅雷
brew cask install mplayerx # 播放器
brew cask install iterm2 # mac上最好用的终端
brew cask install cd-to # 当前目录在终端显示
brew cask install duet # ipad做外接显示器
brew cask install ckb # 海盗船机械键盘驱动
brew cask install shadowsocksx # 翻墙工具
brew cask install firefox # 火狐
brew cask install foxmail # 邮箱客户端
brew cask install rdm # redis 客户端
brew cask install typora # markdown工具
brew cask install macdown
brew cask install cyberduck # ftp工具
brew cask install bearychat
```



## 2、iTerm2

### 2.1 简介

ITERM2 是 MAC 下最好的终端工具。直接去[官网](https://www.iterm2.com/)下载安装包安装即可使用。

### 2.2  iTerm2 常用快捷键

- 切换 tab：⌘+← ，⌘+→  ，⌘+{  ， ⌘+} ，⌘+数字直接定位到该 tab
- 新建 tab：⌘+t
- 顺序切换 pane：⌘+[  ， ⌘+]
- 按方向切换 pane：⌘+Option+方向键
- 切分屏幕：⌘+d 水平切分，⌘+Shift+d 垂直切分
- 智能查找，支持正则查找：⌘+f

## 3、安装OH MY ZSH

### 安装

ZSH 是一种Shell指令集，Mac 自带 ZSH 的安装。但 Oh my zsh 可以让你能更简便的配置 ZSH。 安装方式如下:

```
wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | sh
```

等待安装完成即可

### 配置

设置 zsh 为系统的默认的 shell

```shell
chsh -s /bin/zsh
```

### 更改zsh主题

编辑 `~/.zshrc` ，将文本中的 ZSH_THEME 修改为如下（个人推荐主题：ys）

```shell
    ZSH_THEME="ys"
```

注：主题文件在 `~/.oh-my-zsh/themes` 目录

## 4、安装JDK

#### 安装

通过 HomeBrew 安装 JDK

安装 jdk8 `brew cask info java8`

安装 jdk9 `brew cask info java9`

*也可以通过[官网](http://www.oracle.com/technetwork/java/javase/downloads/index.html)下载安装包安装*

上述两个文件安装完成后，执行下述命令

```shell
echo "alias setJdk9='export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk9.0.4.jdk/Contents/Home'" >> ~/.zshrc
echo "alias setJdk8='export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_161.jdk/Contents/Home'" >> ~/.zshrc
echo "export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_161.jdk/Contents/Home"  >> ~/.zshrc
```

这样在命令行中默认设置当前环境变量为 **JAVA 8** , 当我们需要切换到 **JAVA 9** 时只需在命令行中执行命令 **setJdk9** 即可 。

## 5、安装 Maven

### 安装

Maven 是Java生态中用来构建项目的工具。通过brew安装

```
brew install maven
```

等待安装完成后即可

### 验证

在命令行中输入下述命令验证MAVEN是否正确安装

```shell
$ mvn -v
Apache Maven 3.5.3 (3383c37e1f9e9b3bc3df5050c29c8aff9f295297; 2018-02-25T03:49:05+08:00)
Maven home: /usr/local/Cellar/maven/3.5.3/libexec
Java version: 1.8.0_161, vendor: Oracle Corporation
Java home: /Library/Java/JavaVirtualMachines/jdk1.8.0_161.jdk/Contents/Home/jre
Default locale: zh_CN, platform encoding: UTF-8
OS name: "mac os x", version: "10.13.4", arch: "x86_64", family: "mac"
```

如果有以上输出内容即标识安装完成

### 配置

在 ~/.m2 目录下创建 settings.xml 文件，使用阿里云的 maven 仓库，内容如下

```
<?xml version="1.0" encoding="UTF-8"?>
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd">

    <pluginGroups></pluginGroups>
    <proxies></proxies>
    <servers></servers>
    
    <mirrors>
        <mirror>
            <id>nexus-aliyun</id>
            <mirrorOf>*</mirrorOf>
            <name>Nexus aliyun</name>
            <url>http://maven.aliyun.com/nexus/content/groups/public</url>
        </mirror>
    </mirrors>

    <profiles>
        <profile>
            <id>JDK-1.8</id>
            <activation>
                <activeByDefault>true</activeByDefault>
                <jdk>1.8</jdk>
            </activation>
            <properties>
                <maven.compiler.source>1.8</maven.compiler.source>
                <maven.compiler.target>1.8</maven.compiler.target>
                <maven.compiler.compilerVersion>1.8</maven.compiler.compilerVersion>
            </properties>
        </profile>
    </profiles>

    <activeProfiles>
        <activeProfile>JDK-1.8</activeProfile>
    </activeProfiles>
</settings>

```

## 6、安装 Redis

Redis 是一款基于数据结构的内存数据库。在我们的项目中被用作高速集中式缓存的解决方案。

### 安装

```
brew install redis
```

等待安装完成即可

### 验证

在命令行中输入下述命令查看 reids 版本

```
$ redis-server -v                                                                                                                                                                        
Redis server v=2.8.3 sha=00000000:0 malloc=libc bits=64 build=e836d8ad888e21a1
```

如果有以上输出内容即表示安装完成

## 7、安装MySQL

Mysql 是业界主流的开源关系型数据库。在我们项目中用以持久化用户及系统数据。

### 安装

```
brew install mysql
```

等待安装完成即可

### 验证

```
$ mysql -V
mysql  Ver 14.14 Distrib 5.6.15, for osx10.9 (x86_64) using  EditLine wrapper
```

如果有以上输出内容即表示安装完成

## 8、安装 ElasticSearch

### 安装

Elasticsearch(简称ES）是一款基于lucene的全文搜索中间件。用于处理在大量文本中通过关键字搜索的场景（例如搜索商品、店铺等）。先在下面的链接中下载安装包（已集成相关插件）后解压, 将解压后的文件夹放到你想安装的目录。通过 brew 安装5.6版的 ES： 

```shell
brew install elasticsearch@5.6
```

### 验证 

打开Iterm2，进入 elasticsearch的安装目录，执行以下命令

```
$ ./elasticsearch
```

就可以看到启动日志了

## 9、安装Nginx

### 安装

Nginx 是一款轻量的高性能的Http与反向代理服务器。可被用作转发页面的请求至后台的Tomcat服务器

```
brew install nginx
```

等待安装完成即可

### 验证

在命令行中输入下述命令验证 **Nginx** 是否正确安装 (版本可能有所不同)

```
$ nginx -V
nginx version: nginx/1.6.3
built by clang 6.1.0 (clang-602.0.49) (based on LLVM 3.6.0svn)
TLS SNI support enabled
configure arguments: --prefix=/usr/local/Cellar/nginx/1.6.3 --with-http_ssl_module --with-pcre --with-ipv6 --sbin-path=/usr/local/Cellar/nginx/1.6.3/bin/nginx --with-cc-opt='-I/usr/local/Cellar/pcre/8.36/include -I/usr/local/Cellar/openssl/1.0.2a-1/include' --with-ld-opt='-L/usr/local/Cellar/pcre/8.36/lib -L/usr/local/Cellar/openssl/1.0.2a-1/lib' --conf-path=/usr/local/etc/nginx/nginx.conf --pid-path=/usr/local/var/run/nginx.pid --lock-path=/usr/local/var/run/nginx.lock --http-client-body-temp-path=/usr/local/var/run/nginx/client_body_temp --http-proxy-temp-path=/usr/local/var/run/nginx/proxy_temp --http-fastcgi-temp-path=/usr/local/var/run/nginx/fastcgi_temp --http-uwsgi-temp-path=/usr/local/var/run/nginx/uwsgi_temp --http-scgi-temp-path=/usr/local/var/run/nginx/scgi_temp --http-log-path=/usr/local/var/log/nginx/access.log --error-log-path=/usr/local/var/log/nginx/error.log --with-http_gzip_static_module
```

### 修改配置文件

```text
#user  nobody;
worker_processes  2;

#error_log  logs/error.log;
#error_log  logs/error.log  notice;
#error_log  logs/error.log  info;
#pid        logs/nginx.pid;

events {
    worker_connections  1024;
}

http {
    include       mime.types;
    default_type  application/octet-stream;
    sendfile        on;
    #tcp_nopush     on;
    #keepalive_timeout  0;
    keepalive_timeout  65;
    gzip  on;
    include servers/*.conf;
}
```

## 10、安装 SEQUEL PRO

Sequel Pro 是一款免费的  MySQL 的图形管理工具。

### 安装

在[官网](http://www.sequelpro.com/)可下载最新版本

## 11、安装 IntelliJ IDEA

IntelliJ IDEA 业界公认为最好的 Java 开发工具之一

### 安装

【[官网](https://www.jetbrains.com/idea/)】，网上有很多破解方法，如果资金允许请支持正版

## 12、安装 Zookeeper

### 安装

通过 brew 安装

```shell
brew install zookeeper
```

## 13、LaunchRocket

### 简介

是一个帮助管理Homebrew安装的服务的软件，比如你使用brew安装的Mysql、Redis、MongoDB，LaunchRocket 可以管理这些服务的生命周期和启动方式（自启动、手动启动），传统方式需要使用命令行的命令，而使用LaunchRocket则可以在图形界面中进行管理。

### 安装

brew 安装

```shell
brew cask install launchrocket
```



## 14、一些其他实用软件

### 办公：

```text
markdown 编辑器：BoostNote、Typora、YuWriter、Mou
文本编辑工具：Atom、Visual Studio Code、Sublime
时间/项目管理工具：2Do、OmniPlan、OmniForce
流程图绘制：OmniGraffle
脑图绘制：Xmind、MindNode、iThoughtsX
文稿编辑/演示： KeyNote、Pages、Scrivener、Quiver
状态栏图标隐藏工具：Bartender3
压缩工具：Dr.Unarchiver
技术文档离线阅读：Dash
效率搜索：Alfred3
数据库管理工具：Sequel Pro、Navicat、TablePlus
Git 的 GUI 工具：SourceTree、GitUp
REST 客户端：Postman、Paw
Hosts 切换管理工具：SwitchHosts
```

### 上班必备

```text
社交软件：微信、QQ、钉钉
上班听音乐：网易云音乐、酷我音乐、QQ音乐
```

