---
title: MySQL的一些遗忘点
categories:
  - mysql
tags:
  - mysql
toc: true
date: 2018-10-31 13:59:00
sidebar:

---

## 一、Group By 和 Order By 一起使用

order by 的列，必须是出现在group by 子句里的列 

```sql
SELECT MAX( `id` ) FROM `order`
GROUP BY `order_code` , `created_at`
-- order by 的列，必须是出现在 group by 子句里的列 
ORDER BY `created_at` DESC;
```

## 二、忘记root密码
具体步骤如下：
1. 修改MySQL的配置文件（默认为/etc/my.cnf）,在[mysqld]下添加一行skip-grant-tables

2. 保存配置文件后，重启MySQL服务 service mysqld restart
 
3. 再次进入MySQL命令行 mysql -uroot -p,输入密码时直接回车，就会进入MySQL数据库了，这个时候按照常规流程修改root密码即可。依次输入：
> use mysql;    更改数据库
> UPDATE user SET authentication_string=password("passwd") WHERE USER= 'root';  重设密码,注意我用的是5.7.22的数据库密码存储在authentication_string字段, 之前有的版本存储在password字段,具体看情况吧
> flush privileges;  刷新MySQL的系统权限相关表，以防止更改后拒绝访问；或或者重启MySQL服务器
4. 密码修改完毕后，再按照步骤1中的流程，删掉配置文件中的那行，并且重启MySQL服务，新密码就生效了。

