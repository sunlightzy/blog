---
title: SpringBoot Junit单元测试
categories:
  - spring boot
tags:
  - Junit
  - 单元测试
toc: true
date: 2018-10-31 13:59:00
sidebar:

---

## 一、JUnit中的注解

- @BeforeClass：针对所有测试，只执行一次，且必须为static void
- @Before：初始化方法，执行当前测试类的每个测试方法前执行。
- @Test：测试方法，在这里可以测试期望异常和超时时间
- @After：释放资源，执行当前测试类的每个测试方法后执行
- @AfterClass：针对所有测试，只执行一次，且必须为static void
- @Ignore：忽略的测试方法（只在测试类的时候生效，单独执行该测试方法无效）
- @RunWith:可以更改测试运行器 ，缺省值 org.junit.runner.Runner

**一个单元测试类执行顺序为：**

> @BeforeClass –> @Before –> @Test –> @After –> @AfterClass 

**每一个测试方法的调用顺序为：**

> @Before –> @Test –> @After

