---
title: SpringBoot+H2+Mybatis单元测试整合和坑
categories:
  - spring boot
tags:
  - spring boot
  - h2
  - 单元测试
toc: true
date: 2018-10-31 13:59:00
sidebar:
---

## Maven依赖

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-test</artifactId>
    <scope>test</scope>
</dependency>
<dependency>
    <groupId>com.h2database</groupId>
    <artifactId>h2</artifactId>
    <scope>test</scope>
</dependency>
```



## 配置文件

```yaml
spring:
  datasource:
    driver-class-name: org.h2.Driver
    url: jdbc:h2:mem:testdb;MODE=MYSQL;DB_CLOSE_DELAY=-1;DATABASE_TO_UPPER=false
    username: root # 随便填
    password: 123456 # 随便填
    schema: classpath:db/schema.sql # 建表SQL语句
    data: classpath:db/data.sql # 数据导入SQL语句
    platform: h2
  profiles:
    active: test
```

然后在`src/test/resources`文件夹下面新建一个文件夹`db` ,然后新建 `schema.sql`和`data.sql`

- schema.sql 文件是建表语句,内容不能为空,否则报错
- data.sql文件是数据导入的SQL语句,内容不能为空,否则报错

#### 注意事项

**一、不支持表级别的Comment**

建表SQL如下：

```sql
CREATE TABLE `testTable` (
  `Id` varchar(36) NOT NULL COMMENT '序号',
  `StartArea` int(11) DEFAULT NULL COMMENT '出发区域',
  `ArrivalArea` int(11) DEFAULT NULL COMMENT '目的区域',
  `Updater` varchar(36) DEFAULT NULL COMMENT '更新人',
  `UpdateTime` datetime DEFAULT NULL COMMENT '更新时间' ,
  `Status` int(11) DEFAULT NULL COMMENT '是否删除'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT= '区域路线信息列表' ;
```

列名后面的COMMENT是支持的，但是最后面的 `ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT= '区域路线信息列表'`  中的COMMENT不支持。删掉后面的COMMENT即可。

**二、只支持最普通的引索结构,不支持BTREE引索结构**

```sql
CREATE TABLE `testTable` (
  `Id` varchar(36) NOT NULL COMMENT '序号',
  `StartArea` int(11) DEFAULT NULL COMMENT '出发区域',
  `ArrivalArea` int(11) DEFAULT NULL COMMENT '目的区域',
  `Updater` varchar(36) DEFAULT NULL COMMENT '更新人',
  `UpdateTime` datetime DEFAULT NULL COMMENT '更新时间' ,
  `Status` int(11) DEFAULT NULL COMMENT '是否删除',
  PRIMARY KEY (`Id`) USING BTREE,
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT= '区域路线信息列表' ;
```

这种情况去掉 `USING BTREE` ,使用普通的引索就好了

**三、插入语句的单引号中的\'不支持**

有如下SQL，其中一个字段存的里面带有单引号：

```sql
INSERT INTO `testTable`
VALUES
	(
		'1',
		'部门权限',
		'LoginName=\'{1}\'',
		'1',
		'2',
		NULL,
		NULL,
		'2016-05-27 14:30:49',
		'1',
		'1',
		NULL,
	'1' 
	);
```

MySQL支持双引号包含字符串，可以把内容中包含的单引号改为双引号，但其他情况可能会涉及到业务调整。另外，不能将包含字符串的单引号改为双引号，H2会把双引号中的内容当做列名处理。

**四、H2 的 UNIQUE KEY是数据库级别的**

H2 的 UNIQUE KEY不是表级别的，MySQL是表级别的，转为H2后容易出现UNIQUE KEY重复。删掉UNIQUE KEY或者修改KEY的名称即可。

**五、无法使用子查询**

目前没有办法解决,尽量避免使用吧