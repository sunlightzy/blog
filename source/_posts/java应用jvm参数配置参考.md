---
title: java应用jvm配置参考
categories:
  - jvm
tags:
  - jvm
toc: true
date: 2018-07-05 22:59:00
sidebar:
---

#  java应用jvm配置参考

> 一切配置在亲身测试之后，才比较靠谱。这里给出我们的经验值，仅供参考。

### WEB服务器JVM配置

**容器内存4G**
java内存：4096m * 0.75 = 3072m

```
-server//服务器模式
-Xmx3072m //JVM最大允许分配的堆内存，按需分配
-Xms3072m //JVM初始分配的堆内存，一般和Xmx配置成一样以避免每次gc后JVM重新分配内存。
-XX:NewRatio=1 //表示年轻代与年老代所占比值为1:1 
-XX:+DisableExplicitGC //忽略手动调用GC, System.gc()的调用就会变成一个空调用，完全不触发GC
-XX:+UseConcMarkSweepGC //并发标记清除（CMS）收集器
-XX:+CMSParallelRemarkEnabled //降低标记停顿
-XX:+UseCMSCompactAtFullCollection //在FULL GC的时候对年老代的压缩
-XX:CMSInitiatingOccupancyFraction=70 //使用cms作为垃圾回收使用70％后开始CMS收集
```

**容器内存3G**
java内存：3072m * 0.7 = 2150m

```
-server//服务器模式
-Xmx2150m //JVM最大允许分配的堆内存，按需分配
-Xms2150m //JVM初始分配的堆内存，一般和Xmx配置成一样以避免每次gc后JVM重新分配内存。
-XX:NewRatio=1 //表示年轻代与年老代所占比值为1:1 
-XX:+DisableExplicitGC //忽略手动调用GC, System.gc()的调用就会变成一个空调用，完全不触发GC
-XX:+UseConcMarkSweepGC //并发标记清除（CMS）收集器
-XX:+CMSParallelRemarkEnabled //降低标记停顿
-XX:+UseCMSCompactAtFullCollection //在FULL GC的时候对年老代的压缩
-XX:CMSInitiatingOccupancyFraction=70 //使用cms作为垃圾回收使用70％后开始CMS收集
```

**容器内存2G**
java内存：2048m * 0.7 = 1434m

```
-server//服务器模式
-Xmx1434m //JVM最大允许分配的堆内存，按需分配
-Xms1434m //JVM初始分配的堆内存，一般和Xmx配置成一样以避免每次gc后JVM重新分配内存。
-XX:NewRatio=1 //表示年轻代与年老代所占比值为1:1 
-XX:+DisableExplicitGC //忽略手动调用GC, System.gc()的调用就会变成一个空调用，完全不触发GC
-XX:+UseConcMarkSweepGC //并发标记清除（CMS）收集器
-XX:+CMSParallelRemarkEnabled //降低标记停顿
-XX:+UseCMSCompactAtFullCollection //在FULL GC的时候对年老代的压缩
-XX:CMSInitiatingOccupancyFraction=70 //使用cms作为垃圾回收使用70％后开始CMS收集
```

**容器内存1.5G**
java内存：1536m * 0.7 = 1075m

```
-server//服务器模式
-Xmx1075m //JVM最大允许分配的堆内存，按需分配
-Xms1075m //JVM初始分配的堆内存，一般和Xmx配置成一样以避免每次gc后JVM重新分配内存。
-XX:NewRatio=1 //表示年轻代与年老代所占比值为1:1 
-XX:+DisableExplicitGC //忽略手动调用GC, System.gc()的调用就会变成一个空调用，完全不触发GC
-XX:+UseConcMarkSweepGC //并发标记清除（CMS）收集器
-XX:+CMSParallelRemarkEnabled //降低标记停顿
-XX:+UseCMSCompactAtFullCollection //在FULL GC的时候对年老代的压缩
-XX:CMSInitiatingOccupancyFraction=70 //使用cms作为垃圾回收使用70％后开始CMS收集
```

**容器内存1G**
java内存：1024m * 0.7 = 717m

```
-server//服务器模式
-Xmx717m //JVM最大允许分配的堆内存，按需分配
-Xms717m //JVM初始分配的堆内存，一般和Xmx配置成一样以避免每次gc后JVM重新分配内存。
-XX:NewRatio=1 //表示年轻代与年老代所占比值为1:1 
-XX:+DisableExplicitGC //忽略手动调用GC, System.gc()的调用就会变成一个空调用，完全不触发GC
-XX:+UseConcMarkSweepGC //并发标记清除（CMS）收集器
-XX:+CMSParallelRemarkEnabled //降低标记停顿
-XX:+UseCMSCompactAtFullCollection //在FULL GC的时候对年老代的压缩
-XX:CMSInitiatingOccupancyFraction=70 //使用cms作为垃圾回收使用70％后开始CMS收集
```

### 常用参数说明

**参数设置**
在Java虚拟机的参数中，有3种表示方法：

- 标准参数（-），所有的JVM实现都必须实现这些参数的功能，而且向后兼容；
- 非标准参数（-X），默认jvm实现这些参数的功能，但是并不保证所有jvm实现都满足，且不保证向后兼容；
- 非Stable参数（-XX），此类参数各个jvm实现会有所不同，将来可能会随时取消，需要慎重使用（但是，这些参数往往是非常有用的）；

**常用参数**
现在的JVM运行Java程序时在高效性和稳定性方面做的非常出色。自适应内存管理、垃圾收集、及时编译、动态类加载、锁优化等使得普通程序员几乎不会case内存相关的事情。但JVM仍然大量的参数，使得我们可以针对不同场景进行不同的配置和调优。

*-client*
*-server*
指定JVM的启动模式是client模式还是server模式，具体就是 Java HotSpot Client(Server) VM 版本。目前64位的JDK启动，一定是server模式，会忽略这个参数。

*-Xmn*
设置初始最大的年轻代堆大小。

*-Xms*
设置初始的堆大小。

*-Xmx*
设置最大的内存分配大小。一般的服务端部署，-Xms和-Xmx设置为同样大小。

### 基础回顾

**JVM内存结构**
当代主流虚拟机（Hotspot VM）的垃圾回收都采用“分代回收”的算法。“分代回收”是基于这样一个事实：对象的生命周期不同，所以针对不同生命周期的对象可以采取不同的回收方式，以便提高回收效率。

Hotspot VM将内存划分为不同的物理区，就是“分代”思想的体现。如图所示，JVM内存主要由新生代、老年代、永久代构成。



1. 新生代
   大多数对象在新生代中被创建，其中很多对象的生命周期很短。每次新生代的垃圾回收（又称Minor GC）后只有少量对象存活，所以选用复制算法，只需要少量的复制成本就可以完成回收。
   新生代内又分三个区：一个Eden区，两个Survivor区（一般而言），大部分对象在Eden区中生成。当Eden区满时，还存活的对象将被复制到两个Survivor区（中的一个）。当这个Survivor区满时，此区的存活且不满足“晋升”条件的对象将被复制到另外一个Survivor区。
   对象每经历一次Minor GC，年龄加1，达到“晋升年龄阈值”后，被放到老年代，这个过程也称为“晋升”。
2. 老年代
   在新生代中经历了N次垃圾回收后仍然存活的对象，就会被放到年老代，该区域中对象存活率高。老年代的垃圾回收（又称Major GC）通常使用“标记-清理”或“标记-整理”算法。整堆包括新生代和老年代的垃圾回收称为Full GC（HotSpot VM里，除了CMS之外，其它能收集老年代的GC都会同时收集整个GC堆，包括新生代）。
3. 永久代
   主要存放元数据，例如Class、Method的元信息，与垃圾回收要回收的Java对象关系不大。相对于新生代和年老代来说，该区域的划分对垃圾回收影响比较小。

> jdk1.8中, 永久代最终被移除，方法区移至Metaspace，字符串常量移至Java Heap。永久代的垃圾回收主要两部分：废弃常量和无用类。

**常见垃圾回收器**
不同的垃圾回收器，适用于不同的场景。常用的垃圾回收器：

- 串行（Serial）回收器是单线程的一个回收器，简单、易实现、效率高。
- 并行（ParNew）回收器是Serial的多线程版，可以充分的利用CPU资源，减少回收的时间。
- 吞吐量优先（Parallel Scavenge）回收器，侧重于吞吐量的控制。
- 并发标记清除（CMS，Concurrent Mark Sweep）回收器是一种以获取最短回收停顿时间为目标的回收器，该回收器是基于“标记-清除”算法实现的。

### 实用方法

**jstat** 
jstat可以实时显示本地或远程JVM进程中类装载、内存、垃圾收集、JIT编译等数据（如果要显示远程JVM信息，需要远程主机开启RMI支持）。如果在服务启动时没有指定启动参数-verbose:gc，则可以用jstat实时查看gc情况。

如图，如我本机RemoteMavenServer的GC情况（后两个参数表示，每隔1秒打印1次）：



| 参数 | 解释                     |
| ---- | ------------------------ |
| S0   | 第一个Survivor的使用大小 |
| S1   | 第二个Survivor的使用大小 |
| EU   | 伊甸园区的使用大小       |
| O    | 老年代使用大小           |
| M    | 方法区使用大小           |
| CCS  | 压缩类空间使用大小       |
| YGC  | 年轻代垃圾回收次数       |
| YGCT | 年轻代垃圾回收消耗时间   |
| FGC  | 老年代垃圾回收次数       |
| FGCT | 老年代垃圾回收消耗时间   |
| GCT  | 垃圾回收消耗总时间       |

**gc log**
GC日志是一个很重要的工具，它准确记录了每一次的GC的执行时间和执行结果，通过分析GC日志可以优化堆设置和GC设置，或者改进应用程序的对象分配模式。
不同的垃圾收集器，输出的日志格式各不相同，但也有一些相同的特征。熟悉各个常用垃圾收集器的GC日志，是进行JVM调优的必备一步。

| 参数                               | 说明                                       |
| ---------------------------------- | ------------------------------------------ |
| -XX:+PrintGCDetails                | 打印GC详细信息                             |
| -XX:+PrintGCTimeStamps             | 输出GC的时间戳（以基准时间的形式）         |
| -XX:+PrintGCDateStamps             | 输出GC的时间戳（以日期的形式）             |
| -XX:+PrintHeapAtGC                 | 在进行GC的前后打印出堆的信息               |
| -XX:+PrintTenuringDistribution     | 在进行GC时打印survivor中的对象年龄分布信息 |
| -Xloggc:$CATALINA_HOME/logs/gc.log | 指定输出路径收集日志到日志文件             |

这里以-XX:+UseConcMarkSweepGC日志为例。
-XX:+UseConcMarkSweepGC会指定CMS收集器+ParNew收集器+Serial Old收集器组合，优先使用ParNew收集器+CMS收集器的组合，当出现ConcurrentMode Fail或者Promotion Failed时，则采用ParNew收集器+Serial Old收集器的组合。日志如下：

```
Java HotSpot(TM) 64-Bit Server VM (25.131-b11) for windows-amd64 JRE (1.8.0_131-b11), built on Mar 15 2017 01:23:53 by "java_re" with MS VC++ 10.0 (VS2010)
Memory: 4k page, physical 8303556k(2846816k free), swap 16215992k(7664596k free)
CommandLine flags: -XX:InitialHeapSize=29360128 -XX:MaxHeapSize=29360128 -XX:MaxNewSize=14680064 -XX:MaxTenuringThreshold=6 -XX:OldPLABSize=16 -XX:+PrintGC -XX:+PrintGCDateStamps -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -XX:+UseCompressedClassPointers -XX:+UseCompressedOops -XX:+UseConcMarkSweepGC -XX:-UseLargePagesIndividualAllocation -XX:+UseParNewGC 
2018-05-09T20:53:14.086+0800: 0.590: [GC (Allocation Failure) 2018-08-09T11:53:14.086+0800: 0.590: [ParNew: 11520K->1407K(12928K), 0.0034803 secs] 11520K->2254K(27264K), 0.0039082 secs] [Times: user=0.00 sys=0.00, real=0.00 secs] 
2018-05-09T20:53:14.247+0800: 0.751: [Full GC (System.gc()) 2018-08-09T11:53:14.247+0800: 0.751: [CMS: 846K->1930K(14336K), 0.0103698 secs] 7165K->1930K(27264K), [Metaspace: 5963K->5963K(1056768K)], 0.0104529 secs] [Times: user=0.02 sys=0.00, real=0.01 secs] 
2018-05-09T20:53:14.292+0800: 0.795: [GC (Allocation Failure) 2018-08-09T11:53:14.292+0800: 0.795: [ParNew: 11519K->1199K(12928K), 0.0085679 secs] 13450K->3129K(27264K), 0.0086244 secs] [Times: user=0.00 sys=0.00, real=0.01 secs] 
2018-05-09T20:53:14.333+0800: 0.836: [GC (Allocation Failure) 2018-08-09T11:53:14.333+0800: 0.836: [ParNew: 12719K->300K(12928K), 0.0002620 secs] 14649K->2230K(27264K), 0.0003041 secs] [Times: user=0.00 sys=0.00, real=0.00 secs] 
2018-05-09T20:53:14.364+0800: 0.867: [GC (Allocation Failure) 2018-08-09T11:53:14.364+0800: 0.867: [ParNew: 11820K->75K(12928K), 0.0002787 secs] 13750K->2005K(27264K), 0.0003223 secs] [Times: user=0.00 sys=0.00, real=0.00 secs] 
# 并发标记周期开始，根区域扫描
2018-05-09T20:59:47.982+0800: 9.634: [GC concurrent-root-region-scan-start]
2018-05-09T20:59:47.982+0800: 9.652: [GC concurrent-root-region-scan-end, 0.0184308 secs]
# 并发标记
2018-05-09T20:59:47.982+0800: 9.652: [GC concurrent-mark-start]
2018-05-09T20:59:47.982+0800: 9.693: [GC concurrent-mark-end, 0.0406187 secs]
# 重新标记
2018-05-09T20:59:47.982+0800: 9.695: [GC remark 9.695: [Finalize Marking, 0.0005100 secs] 9.695: [GC ref-proc, 0.0003461 secs] 9.696: [Unloading, 0.0069466 secs], 0.0082011 secs]
 [Times: user=0.02 sys=0.00, real=0.01 secs] 
# 独占清理
2018-05-09T20:59:47.982+0800: 9.703: [GC cleanup 25M->21M(1024M), 0.0027119 secs]
 [Times: user=0.00 sys=0.01, real=0.00 secs] 
# 并发清理
2018-05-09T20:59:47.982+0800: 9.706: [GC concurrent-cleanup-start]
2018-05-09T20:59:47.982+0800: 9.706: [GC concurrent-cleanup-end, 0.0000167 secs]
2018-05-09T20:54:39.299+0800: 85.803: [Full GC (System.gc()) 2018-08-09T11:54:39.299+0800: 85.803: [CMS: 1930K->1832K(14336K), 0.0089015 secs] 12748K->1832K(27264K), [Metaspace: 6035K->6035K(1056768K)], 0.0089724 secs] [Times: user=0.00 sys=0.00, real=0.01 secs] 

Heap
 par new generation   total 12928K, used 227K [0x00000000fe400000, 0x00000000ff200000, 0x00000000ff200000)
  eden space 11520K,   1% used [0x00000000fe400000, 0x00000000fe438cd8, 0x00000000fef40000)
  from space 1408K,   0% used [0x00000000fef40000, 0x00000000fef40000, 0x00000000ff0a0000)
  to   space 1408K,   0% used [0x00000000ff0a0000, 0x00000000ff0a0000, 0x00000000ff200000)
 concurrent mark-sweep generation total 14336K, used 1832K [0x00000000ff200000, 0x0000000100000000, 0x0000000100000000)
 Metaspace       used 6045K, capacity 6252K, committed 6400K, reserved 1056768K
  class space    used 691K, capacity 761K, committed 768K, reserved 1048576K
```

第三行把当前使用的JVM参数打印出来，其中，-XX:MaxTenuringThreshold=6是指对象从新生代晋升到老年代需要对象年龄达到6岁，即经过6次GC。

第四行是新生代Young区的GC，首先是GC发生的时间。然后是GC发生的原因GC (Allocation Failure)，对象分配失败。[ParNew: 11520K->1407K(12928K), 0.0034803 secs]表示新生代回收前是11520K，回收后是1407K，新生代总大小12928K，回收耗时0.0034803 secs。11520K->2254K(27264K), 0.0039082 secs表示回收前堆大小11520K，回收后堆大小2254K，堆的总大小27264K。

第五行是老年代Old区的GC，首先是GC发生的时间。然后是GC发生的原因System.gc()，由于代码调用。[CMS: 846K->1930K(14336K), 0.0103698 secs]表示回收前老年代是846K，回收后1930K，老年代总大小14336K，回收耗时0.0103698 secs。7165K->1930K(27264K)表示回收前堆大小7165K，回收后堆大小1930K，堆的总大小27264K。

后面有一次并发标记周期，设置参数-XX:InitiatingHeapOccupancyPercent的值，可以指定堆占有率达到百分之多少时，触发并发标记，默认值是45%。

最后打印出了堆的整体使用情况，分为新生代、老年代、元空间。

### 调优方法

**请记住下面的原则：**

1. 多数的Java应用不需要在服务器上进行GC优化；
2. 多数导致GC问题的Java应用，都不是因为我们参数设置错误，而是代码问题；
3. 在应用上线之前，先考虑将机器的JVM参数设置到最优（最适合）；
4. 减少创建对象的数量；
5. 减少使用全局变量和大对象；
6. GC优化是到最后不得已才采用的手段；

> 在实际使用中，分析GC情况优化代码比优化GC参数要多得多。

**GC优化的目的有两个：**

1. 将转移到老年代的对象数量降低到最小；
2. 减少full GC的执行时间；

**为了达到上面的目的，一般地，你需要做的事情有：**

1. 减少使用全局变量和大对象；
2. 调整新生代的大小到最合适；
3. 设置老年代的大小为最合适；
4. 选择合适的GC收集器；

**进行监控和调优的一般步骤：**

1. 监控GC的状态
   使用各种JVM工具，查看当前日志，分析当前JVM参数设置，并且分析当前堆内存快照和gc日志，根据实际的各区域内存划分和GC执行时间，觉得是否进行优化；
2. 分析结果，判断是否需要优化
   如果各项参数设置合理，系统没有超时日志出现，GC频率不高，GC耗时不高，那么没有必要进行GC优化；如果GC时间超过1-3秒，或者频繁GC，则必须优化；

> 注：如果满足下面的指标，则一般不需要进行GC调优：
>
> Minor GC执行时间不到50ms；
> Minor GC执行不频繁，约10秒一次；
> Full GC执行时间不到1s；
> Full GC执行频率不算频繁，不低于10分钟1次；

1. 调整GC类型和内存分配
   如果内存分配过大或过小，或者采用的GC收集器比较慢，则应该优先调整这些参数，并且先找1台或几台机器进行beta，然后比较优化过的机器和没有优化的机器的性能对比，并有针对性的做出最后选择；
2. 不断的分析和调整
   通过不断的试验和试错，分析并找到最合适的参数
3. 全面应用参数
   如果找到了最合适的参数，则将这些参数应用到所有服务器，并进行后续跟踪。