---

title: Centos 7 环境构建
categories:
  - centos
tags:
  - centos
toc: true
date: 2018-08-05 22:59:00
sidebar:

---

# Centos 7 环境构建

## 目录准备

```shell
在~目录下建立如下目录结构
|-apps
	|-bin		存放可执行文件
	|- cellar		存放安装文件 lib
	|-data		存放应用数据
	|- etc		存放配置文件
	|- sbin		存放超管执行文件
	|- src		存放源码文件
	|- var		存放运行时产生文件，log、pid
```



```shell
# 构建目录
cd ~
mkdir apps
cd apps
mkdir src bin sbin data var cellar etc
```



## 环境准备

### 必装软件

Git、JDK、Maven、Docker、Nginx、Redis、MySQL、Zookeeper、Node

#### GIT 

##### 安装

```shell
# 更新 yum 包
sudo yum update
# 安装 git
# 检查是否自带的 git
yum list installed | grep git
# 如果有，卸载自带的 git
yum remove git
#安装gcc
yum install gcc
#安装其它所需的包
yum install curl-devel expat-devel gettext-devel openssl-devel zlib-devel 
yum install gcc-c++ perl-ExtUtils-MakeMaker
yum install autoconf automake libtool
# 进入源码目录
cd ~/apps/src
# 下载 git 源码，使用源码安装
wget https://github.com/git/git/archive/v2.17.0.tar.gz
# 解压
tar zxvf v2.17.0.tar.gz
cd git-2.17.0
# 配置源码安装
make configure
./configure --help
# 比较有用，下面的配置都是来自于 help ，返回响应省略
./configure --prefix=/root/apps/cellar/git --sbindir=/root/apps/sbin/git --bindir=/root/apps/bin/git --sysconfdir=/root/apps/etc/git/git.conf --datadir=/root/apps/data/git -docdir=/root/apps/data/git/doc
# make 和 install
make && make install
# 清理 make 文件
make clean
make distclean
# 设置环境变量
vi /etc/profile
#git 添加到文件最后
export PATH="$PATH:/root/apps/bin/git"
# 生效配置的变量
source /etc/profile
# 查看版本
git --version
```

##### 配置

```
# 创建用户
git config --global user.name "你的名字"
git config --global user.email "你的邮箱"
# 创建秘钥
ssh-keygen -t rsa -C "你的邮箱"
# 获取公钥，将公钥添加到 git 加入 git 服务器
```

#### JDK安装

```shell
# 检查并卸载OpenJDK
# 检查命令：
java -version
rpm -qa | grep java
# 卸载 OpenJDK
rpm -e --nodeps java-1.8.0-openjdk-headless-debug-1.8.0.171-3.b10.el6_9.x86_64
rpm -e --nodeps java-1.8.0-openjdk-headless-1.8.0.171-3.b10.el6_9.x86_64
rpm -e --nodeps java-1.8.0-openjdk-demo-debug-1.8.0.171-3.b10.el6_9.x86_64
rpm -e --nodeps java-1.8.0-openjdk-debug-1.8.0.171-3.b10.el6_9.x86_64
rpm -e --nodeps java-1.8.0-openjdk-devel-debug-1.8.0.171-3.b10.el6_9.x86_64
rpm -e --nodeps java-1.8.0-openjdk-src-1.8.0.171-3.b10.el6_9.x86_64
rpm -e --nodeps java-1.8.0-openjdk-demo-1.8.0.171-3.b10.el6_9.x86_64
rpm -e --nodeps java-1.8.0-openjdk-src-debug-1.8.0.171-3.b10.el6_9.x86_64
rpm -e --nodeps java-1.8.0-openjdk-1.8.0.171-3.b10.el6_9.x86_64
rpm -e --nodeps java-1.8.0-openjdk-devel-1.8.0.171-3.b10.el6_9.x86_64
# 下面两个可以不用删
rpm -e --nodeps java-1.8.0-openjdk-javadoc-debug-1.8.0.171-3.b10.el6_9.noarch
rpm -e --nodeps java-1.8.0-openjdk-javadoc-1.8.0.171-3.b10.el6_9.noarch
# 验证删除是否完成
java -version
# 如果还没有删除，则用yum -y remove去删除他们

# 安装
# 去官网下载指定版本的 jdk 压缩包解压
# 命令介绍：
# rpm 　　　　管理套件  
# -e　　　　　删除指定的套件
# --nodeps　　不验证套件档的相互关联性
# 配置环境变量
# 安装完成后需要配置一下环境变量，编辑/etc/profile文件：
vi /etc/profile
# 在文件尾部添加如下配置：
export JAVA_HOME=/tools/jdk1.8.0_171
export CLASSPATH=.:$JAVA_HOME/jre/lib/rt.jar:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar 
export PATH=$PATH:$JAVA_HOME/bin
# 使变量生效
source /etc/profile
```

#### Maven 安装

```shell
# 下载压缩包
wget http://mirrors.shu.edu.cn/apache/maven/maven-3/3.5.3/binaries/apache-maven-3.5.3-bin.tar.gz
# 版本 apache-maven-3.5.3-bin.tar.gz
tar -xvf apache-maven-3.5.3-bin.tar.gz
mv apache-maven-3.5.3 /usr/local/maven
# 文件存放好之后，设置环境变量，
vi /etc/profile
#写入环境变量
export M2_HOME=/usr/local/maven
export PATH=$PATH:$M2_HOME/bin
# 再执行
source /etc/profile
# 验证
mvn -v
```




#### Docker 安装

Docker 要求 CentOS 系统的内核版本高于 3.10 ，查看本页面的前提条件来验证你的CentOS 版本是否支持 Docker 。

[官方安装文档](https://docs.docker.com/install/linux/docker-ce/centos/#install-from-a-package)

**方式一：**通过 yum 安装

```shell
# 通过 uname -r 命令查看你当前的内核版本
uname -r
# 卸载旧版本(如果安装过旧版本的话)
sudo yum remove docker docker-client docker-client-latest docker-common docker-latest docker-latest-logrotate docker-logrotate docker-selinux docker-engine-selinux docker-engine
# 安装需要的软件包， yum-util 提供yum-config-manager功能，另外两个是devicemapper驱动依赖的
sudo yum install -y yum-utils device-mapper-persistent-data lvm2
# 设置稳定的镜像，使用国内阿里云的
sudo yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
# 查看哪些 docker 版本可用，都是稳定版
yum list docker-ce --showduplicates | sort -r
# 安装 docker
sudo yum install docker-ce
# 安装指定版本使用 sudo yum install docker-ce-<VERSION STRING>
# 启动 docker
sudo systemctl start docker
# 开机启动
sudo systemctl enable docker
```

**方式二：**通过 rpm 安装包安装

1. 去 <https://download.docker.com/linux/centos/7/x86_64/stable/Packages/> 下载你要的版本的 `.rpm` 文件

2. 指定下载的文件路径去安装 docker

```shell
sudo yum install /path/to/package.rpm
```

3. 启动 docker

```shell
sudo systemctl start docker
# 开机启动
sudo systemctl enable docker
```

##### 设置mirror 

使用清华源  <https://lug.ustc.edu.cn/wiki/mirrors/help/docker>          

新版的 Docker 使用 /etc/docker/daemon.json 来配置 Daemon ，在该配置文件中加入（没有该文件的话，请先创建一个）

```json
{"registry-mirrors":["http://13694f87.m.daocloud.io"]} 
```

如果 docker 不能 pull ，设置其它镜像参考：<http://www.datastart.cn/tech/2016/09/28/docker-mirror.html>

最后，通过 hello-world 验证是否安装成功

``` shell
sudo docker run hello-world
```

#### Zookeeper

```shell
# 下载
wget http://mirror.bit.edu.cn/apache/zookeeper/stable/zookeeper-3.4.12.tar.gz
tar zxvf zookeeper-3.4.12.tar.gz
# 新建zookeeper配置文件,Zookeeper需要一个名为zoo.cfg的配置文件，我们解压后，得到的是官方的示例文件，名为zoo_sample.cfg，这个文件在zookeeper根目录的conf子目录下
# 进入 config 文件夹
cd zookeeper-3.4.10/conf/
cp zoo_sample.cfg  zoo.cfg
# 用 vim 打开 zoo.cfg 文件并修改其内容为如下：
    # The number of milliseconds of each tick
 
    # zookeeper 定义的基准时间间隔，单位：毫秒
    tickTime=2000
 
    # The number of ticks that the initial 
    # synchronization phase can take
    initLimit=10
    # The number of ticks that can pass between 
    # sending a request and getting an acknowledgement
    syncLimit=5
    # the directory where the snapshot is stored.
    # do not use /tmp for storage, /tmp here is just 
    # example sakes.
    # dataDir=/tmp/zookeeper
 
    # 数据文件夹
    dataDir=/root/apps/data/zookeeper
 
    # 日志文件夹
    dataLogDir=/root/apps/var/zookeeper/logs
 
    # the port at which the clients will connect
    # 客户端访问 zookeeper 的端口号
    clientPort=2181
 
    # the maximum number of client connections.
    # increase this if you need to handle more clients
    #maxClientCnxns=60
    #
    # Be sure to read the maintenance section of the 
    # administrator guide before turning on autopurge.
    #
    # http://zookeeper.apache.org/doc/current/zookeeperAdmin.html#sc_maintenance
    #
    # The number of snapshots to retain in dataDir
    #autopurge.snapRetainCount=3
    # Purge task interval in hours
    # Set to "0" to disable auto purge feature
    #autopurge.purgeInterval=1
# 用 vim 打开 /etc/ 目录下的配置文件 profile：
vim /etc/profile
# 添加
export ZOOKEEPER_HOME=/root/apps/zookeeper
export PATH=$ZOOKEEPER_HOME/bin:$PATH
export PATH
# 使 /etc/ 目录下的 profile 文件即可生效：
source /etc/profile
# 启动
zkServer.sh   start
# 重新启动
zkServer.sh   restart
# 执行命令查看zookeeper状态：
zkServer.sh   status
```

#### Nginx

```shell
# gcc 安装
# 安装 nginx 需要先将官网下载的源码进行编译，编译依赖 gcc 环境，如果没有 gcc 环境，则需要安装：
yum install gcc-c++
# PCRE pcre-devel 安装
# PCRE(Perl Compatible Regular Expressions) 是一个Perl库，包括 perl 兼容的正则表达式库。nginx 的 http 模块使用 pcre 来解析正则表达式，所以需要在 linux 上安装 pcre 库，pcre-devel 是使用 pcre 开发的一个二次开发库。nginx也需要此库。命令：
yum install -y pcre pcre-devel
# zlib 安装
# zlib 库提供了很多种压缩和解压缩的方式， nginx 使用 zlib 对 http 包的内容进行 gzip ，所以需要在 Centos 上安装 zlib 库。
yum install -y zlib zlib-devel
# OpenSSL 安装
# OpenSSL 是一个强大的安全套接字层密码库，囊括主要的密码算法、常用的密钥和证书封装管理功能及 SSL 协议，并提供丰富的应用程序供测试或其它目的使用。
# nginx 不仅支持 http 协议，还支持 https（即在ssl协议上传输http），所以需要在 Centos 安装 OpenSSL 库。
yum install -y openssl openssl-devel

# nginx 下载
wget -c https://nginx.org/download/nginx-1.14.0.tar.gz
# 解压
# 依然是直接命令：
tar -zxvf nginx-1.14.0.tar.gz
cd nginx-1.14.0

# 配置
# 其实在 nginx-1.10.1 版本中你就不需要去配置相关东西，默认就可以了。当然，如果你要自己配置目录也是可以的。
# 1.使用默认配置
./configure
#2.自定义配置（不推荐）
./configure \
--prefix=/usr/local/nginx \
--conf-path=/usr/local/nginx/conf/nginx.conf \
--pid-path=/usr/local/nginx/conf/nginx.pid \
--lock-path=/var/lock/nginx.lock \
--error-log-path=/var/log/nginx/error.log \
--http-log-path=/var/log/nginx/access.log \
--with-http_gzip_static_module \
--http-client-body-temp-path=/var/temp/nginx/client \
--http-proxy-temp-path=/var/temp/nginx/proxy \
--http-fastcgi-temp-path=/var/temp/nginx/fastcgi \
--http-uwsgi-temp-path=/var/temp/nginx/uwsgi \
--http-scgi-temp-path=/var/temp/nginx/scgi
# 注：将临时文件目录指定为/var/temp/nginx，需要在/var下创建temp及nginx目录
# 编译安装
make
make install
# 查找安装路径：
whereis nginx
# 启动、停止nginx
cd /usr/local/nginx/sbin/
./nginx 
./nginx -s stop
./nginx -s quit
./nginx -s reload
# ./nginx -s quit:此方式停止步骤是待nginx进程处理任务完毕进行停止。
# ./nginx -s stop:此方式相当于先查出nginx进程id再使用kill命令强制杀掉进程。

# 查询nginx进程：
ps aux|grep nginx

# 重启 nginx
# 1.先停止再启动（推荐）：
# 对 nginx 进行重启相当于先停止再启动，即先执行停止命令再执行启动命令。如下：
./nginx -s quit
./nginx
# 2.重新加载配置文件：
# 当 ngin x的配置文件 nginx.conf 修改后，要想让配置生效需要重启 nginx，使用-s reload不用先停止 ngin x再启动 nginx 即可将配置信息在 nginx 中生效，如下：
./nginx -s reload

# 开机自启动
# 即在rc.local增加启动代码就可以了。
vi /etc/rc.local
# 增加一行 /usr/local/nginx/sbin/nginx
# 设置执行权限：
chmod 755 rc.local
```



#### Redis

```shell
$ wget http://download.redis.io/releases/redis-4.0.9.tar.gz
$ tar xzf redis-4.0.9.tar.gz
$ cd redis-4.0.9
$ make
$ src/redis-server
$ src/redis-cli
redis> set foo bar
OK
redis> get foo
"bar"
```



#### MySQL

##### 1、配置YUM源

在MySQL官网中下载YUM源rpm安装包：http://dev.mysql.com/downloads/repo/yum/ 
![MySQL YUM源下载地址](https://www.linuxidc.com/upload/2016_09/160918124758191.png)

```
# 下载mysql源安装包
shell> wget http://dev.mysql.com/get/mysql57-community-release-el7-8.noarch.rpm
# 安装mysql源
shell> yum localinstall mysql57-community-release-el7-8.noarch.rpm
```

检查mysql源是否安装成功

```
shell> yum repolist enabled | grep "mysql.*-community.*"
```

![检查mysql源安装是否正确](https://www.linuxidc.com/upload/2016_09/160918124758192.png) 
看到上图所示表示安装成功。 
可以修改`vim /etc/yum.repos.d/mysql-community.repo`源，改变默认安装的mysql版本。比如要安装5.6版本，将5.7源的enabled=1改成enabled=0。然后再将5.6源的enabled=0改成enabled=1即可。改完之后的效果如下所示： 
![这里写图片描述](https://www.linuxidc.com/upload/2016_09/160918124758197.jpg)

##### 2、安装MySQL

```
shell> yum install mysql-community-server
```

##### 3、启动MySQL服务

```
shell> systemctl start mysqld
```

查看MySQL的启动状态

```
shell> systemctl status mysqld
● mysqld.service - MySQL Server
   Loaded: loaded (/usr/lib/systemd/system/mysqld.service; disabled; vendor preset: disabled)
   Active: active (running) since 五 2016-06-24 04:37:37 CST; 35min ago
 Main PID: 2888 (mysqld)
   CGroup: /system.slice/mysqld.service
           └─2888 /usr/sbin/mysqld --daemonize --pid-file=/var/run/mysqld/mysqld.pid

6月 24 04:37:36 localhost.localdomain systemd[1]: Starting MySQL Server...
6月 24 04:37:37 localhost.localdomain systemd[1]: Started MySQL Server.
```

##### 4、开机启动

```
shell> systemctl enable mysqld
shell> systemctl daemon-reload
```

##### 5、修改root本地登录密码

mysql安装完成之后，在/var/log/mysqld.log文件中给root生成了一个默认密码。通过下面的方式找到root默认密码，然后登录mysql进行修改：

```
shell> grep 'temporary password' /var/log/mysqld.log
```

![root默认密码](https://www.linuxidc.com/upload/2016_09/160918124758193.png)

```
shell> mysql -uroot -p
mysql> ALTER USER 'root'@'localhost' IDENTIFIED BY 'MyNewPass4!'; 
```

或者

```
mysql> set password for 'root'@'localhost'=password('MyNewPass4!'); 
```

注意：mysql5.7默认安装了密码安全检查插件（validate_password），默认密码检查策略要求密码必须包含：大小写字母、数字和特殊符号，并且长度不能少于8位。否则会提示ERROR 1819 (HY000): Your password does not satisfy the current policy requirements错误，如下图所示： 
![密码策略提示](https://www.linuxidc.com/upload/2016_09/160918124758194.png)

通过msyql环境变量可以查看密码策略的相关信息：

```
mysql> show variables like '%password%';
```

![mysql密码策略](https://www.linuxidc.com/upload/2016_09/160918124758195.png) 
validate_password_policy：密码策略，默认为MEDIUM策略 
validate_password_dictionary_file：密码策略文件，策略为STRONG才需要 
validate_password_length：密码最少长度 
validate_password_mixed_case_count：大小写字符长度，至少1个 
validate_password_number_count ：数字至少1个 
validate_password_special_char_count：特殊字符至少1个 
*上述参数是默认策略MEDIUM的密码检查规则。*

共有以下几种密码策略：

| 策略        | 检查规则                                                     |
| ----------- | ------------------------------------------------------------ |
| 0 or LOW    | Length                                                       |
| 1 or MEDIUM | Length; numeric, lowercase/uppercase, and special characters |
| 2 or STRONG | Length; numeric, lowercase/uppercase, and special characters; dictionary file |

MySQL官网密码策略详细说明：http://dev.mysql.com/doc/refman/5.7/en/validate-password-options-variables.html#sysvar_validate_password_policy

##### 修改密码策略

在/etc/my.cnf文件添加validate_password_policy配置，指定密码策略

```
# 选择0（LOW），1（MEDIUM），2（STRONG）其中一种，选择2需要提供密码字典文件
validate_password_policy=0
```

如果不需要密码策略，添加my.cnf文件中添加如下配置禁用即可：

```
validate_password = off
```

重新启动mysql服务使配置生效：

```
systemctl restart mysqld
```

##### 6、添加远程登录用户

默认只允许root帐户在本地登录，如果要在其它机器上连接mysql，必须修改root允许远程连接，或者添加一个允许远程连接的帐户，为了安全起见，我添加一个新的帐户：

```
mysql> GRANT ALL PRIVILEGES ON *.* TO 'yangxin'@'%' IDENTIFIED BY 'Yangxin0917!' WITH GRANT OPTION;
```

##### 7、配置默认编码为utf8

修改/etc/my.cnf配置文件，在[mysqld]下添加编码配置，如下所示：

```
[mysqld]
character_set_server=utf8
init_connect='SET NAMES utf8'
```

重新启动mysql服务，查看数据库默认编码如下所示：

![mysql默认编码](https://www.linuxidc.com/upload/2016_09/160918124758196.png)

------

**默认配置文件路径：** 
配置文件：/etc/my.cnf 
日志文件：/var/log//var/log/mysqld.log 
服务启动脚本：/usr/lib/systemd/system/mysqld.service 
socket文件：/var/run/mysqld/mysqld.pid



#### Node

1、下载源码，你需要下载最新的Nodejs版本，本文以node-v8.11.2.tar.gz为例:

```shell
wget https://npm.taobao.org/mirrors/node/v8.11.2/node-v8.11.2.tar.gz
```

2、解压源码

```shell
tar zxvf node-v8.11.2.tar.gz
```

3、 编译安装

```shell
cd node-v8.11.2
./configure --prefix=/root/apps/cellar/node
make
make install
```

4、 配置NODE_HOME，进入profile编辑环境变量

```shell
vim /etc/profile
```

设置nodejs环境变量，在 **\*export PATH USER LOGNAME MAIL HOSTNAME HISTSIZE HISTCONTROL*** 一行的上面添加如下内容:

```shell
#set for nodejs
export NODE_HOME=/root/apps/cellar/node
export PATH=$NODE_HOME/bin:$PATH
```

:wq保存并退出，编译/etc/profile 使配置生效

```shell
source /etc/profile
```

验证是否安装配置成功

```shell
node -v
```

输出 版本号表示配置成功

npm模块安装路径

```shell
/root/apps/cellar/node/lib/node_modules/
```



### 可选装

ElasticSearch5.x、Kibana5.x、RocketMQ、zsh、MongoDB、Kafka、RabbitMQ、MariaDB