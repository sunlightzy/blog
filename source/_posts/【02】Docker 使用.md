---

title: Docker使用
categories:
  - docker
tags:
  - docker
toc: true
date: 2018-08-11 22:59:00
sidebar:

---
# Docker使用

#### **一. docker使用**

##### 　　1. docker ps 查看运行中的容器

##### 　　2. docker images 查看docker镜像

##### 　　3. docker rm id(容器id)  删除容器（容器id可以通过docker ps查看，容器必须停止后才能删除）

###### 　　　　3.1 删除全部的容器 docker rm `docker ps -a -q`

##### 　　4. docker stop  id(容器id) 停止容器运行

##### 　　5. docker rmi  id(镜像id) 删除镜像

##### 　　6. docker pull ubuntu:16.04(镜像名称:版本号) 下载镜像

##### 　　7. docker run -it ubuntu:16.04 创建并运行容器容器

　　　　-t 表示在新容器内指定一个伪终端或终端

　　　　-i 表示允许我们对容器内的 (STDIN) 进行交互

　　　　-p 指定映射端口

　　　　-d 在后台运行容器并打印容器ID

###### 　　　　7.1 docker run -dit ubuntu:16.04 创建并后台运行容器

###### 　　　　7.2 docker run -ditp 8080:8080（主机端口:容器端口） ubuntu:16.04 创建并后台运行容器且映射容器的端口

##### 　　8. docker attach id(容器id) 进入正在运行中的容器环境

##### 　　9. 退出容器

###### 　　　　9.1 exit 直接退出容器并终止容器运行

###### 　　　　9.2 [ctrl+p]+[ctrl+q]（快捷键） 退出容器，但是不会终止容器运行

##### 　　10. docker commit -m'版本标识' id(容器id) ubuntu:16.04(镜像与版本号)   提交镜像且生成镜像（可以通过该命令把搭建好的容器打包成一个新的镜像或者覆盖原镜像（即是修改原镜像内容，生成的镜像名与版本号相同就可以直接覆盖）） 