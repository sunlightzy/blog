---

title: 我的ElasticSearch命令简记
categories:
  - elasticsearch
tags:
  - elasticsearch
toc: true
date: 2018-10-31 13:59:00
sidebar:

---


## 常用简单命令

### 条件删除数据

```sh
# 条件删除
curl -XPOST "http://localhost:9200/opt-log-index/opt-log-type/_delete_by_query" -H 'Content-Type: application/json' -d'
{
  "query": {
    "match_all": {}
  }
}'
```
### 删除index和数据

```sh
curl -XDELETE "http://localhost:9200/opt-log-index"
```

### 获取 mapping 结构
```sh
# 获取所有的index的mapping
curl -XGET "http://localhost:9200/_mapping"

# 获取指定的index的mapping结构
curl -XGET "http://localhost:9200/opt-log-index/_mapping"
```

### 创建index和mapping
```sh
# 创建index和mapping
curl -XPUT "http://localhost:9200/opt-log-index" -H 'Content-Type: application/json' -d'
{
  "settings":{
    "analysis":{
      "analyzer":{
        "ik":{
          "tokenizer":"ik_max_word"
        }
      }
    }
  },
  "mappings":{
    "opt-log-type":{
      "properties":{
        "id":{
          "type":"keyword"
        },
        "operator":{
          "type":"keyword"
        },
        "role":{
          "type":"keyword"
        },
        "operatorName":{
          "type":"text",
          "analyzer":"ik",
          "search_analyzer":"ik_max_word"
        },
        "remark":{
          "type":"keyword"
        },
        "operateType":{
          "type":"keyword"
        },
        "txId":{
          "type":"long"
        },
        "schemaName":{
          "type":"text",
          "analyzer":"ik",
          "search_analyzer":"ik_max_word"
        },
        "tableName":{
          "type":"text",
          "analyzer":"ik",
          "search_analyzer":"ik_max_word"
        },
        "afterData":{
          "type":"keyword"
        },
        "beforeData":{
          "type":"keyword"
        },
        "changeFields":{
          "type":"keyword"
        },
        "createdAt":{
          "type":"date",
          "format":"yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"
        },
        "updatedAt":{
          "type":"date",
          "format":"yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"
        }
      }
    }
  }
}'
```

### 查询数据

#### 查询所有数据

```sh
curl -XGET "http://localhost:9200/opt-log-index/opt-log-type/_search" -H 'Content-Type: application/json' -d'
{
  "from": 0, 
  "size": 20, 
  "query": {
    "bool": {
      "must": [
        {
          "match_all": {}
        }
      ]
    }
  },
  "_source": {
    "excludes": [
      "beforeData"
    ]
  }
}'
```

#### 精准匹配

```sh
curl -XGET "http://localhost:9200/opt-log-index/opt-log-type/_search" -H 'Content-Type: application/json' -d'
{
  "query": {
    "bool": {
      "must": [
        {
          "term": {
            "tableName": "parana_item_detail"
          }
        }
      ]
    }
  },
  "_source": {
    "excludes": [
      "beforeData","schemaName"
    ]
  },
  "sort": [
    {
      "createdAt": {
        "order": "desc"
      }
    }
  ]
}'
```

