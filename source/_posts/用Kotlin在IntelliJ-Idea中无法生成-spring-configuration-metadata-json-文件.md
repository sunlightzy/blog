---
title: Kotlin在IntelliJ Idea中无法生成 spring-configuration-metadata.json 文件
categories:
  - kotlin
tags:
  - kotlin
  - spring boot
toc: true
date: 2018-09-05 19:12:09
sidebar:
---

# 问题描述

在百度搜索关键词,搜索到了 Stack Overflow 有相关问题

> spring-configuration-metadata.json file is not generated in IntelliJ Idea for Kotlin @ConfigurationProperties class



原文链接: 

> <https://stackoverflow.com/questions/37858833/spring-configuration-metadata-json-file-is-not-generated-in-intellij-idea-for-ko>



按照里面的方法试了一下,失败了,然后继续百度,在spring-boot的官方文档中找到了相关线索, 直达链接:

> <https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-kotlin.html#boot-features-kotlin-configuration-properties>



在spring官方文档中找到了kotlin的官方示例,链接地址:

> <https://kotlinlang.org/docs/reference/kapt.html#using-in-maven>



下面是我参考上面的文档所得出来的可用方案



# 解决方案



## 一、添加插件



在**pom**文件中添加插件,没有写版本号是因为项目继承了`spring-boot-starter-parent`

```xml
<plugins>
    <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <configuration>
            <proc>none</proc>
            <source>${java.version}</source>
            <target>${java.version}</target>
        </configuration>
        <executions>
            <!-- Replacing default-compile as it is treated specially by maven -->
            <execution>
                <id>default-compile</id>
                <phase>none</phase>
            </execution>
            <!-- Replacing default-testCompile as it is treated specially by maven -->
            <execution>
                <id>default-testCompile</id>
                <phase>none</phase>
            </execution>
            <execution>
                <id>java-compile</id>
                <phase>compile</phase>
                <goals>
                    <goal>compile</goal>
                </goals>
            </execution>
            <execution>
                <id>java-test-compile</id>
                <phase>test-compile</phase>
                <goals>
                    <goal>testCompile</goal>
                </goals>
            </execution>
        </executions>
    </plugin>

    <plugin>
        <artifactId>kotlin-maven-plugin</artifactId>
        <groupId>org.jetbrains.kotlin</groupId>
        <configuration>
            <args>
                <arg>-Xjsr305=strict</arg>
            </args>
            <compilerPlugins>
                <plugin>spring</plugin>
            </compilerPlugins>
            <jvmTarget>${java.version}</jvmTarget>
        </configuration>
        <executions>
            <execution>
                <id>kapt</id>
                <goals>
                    <goal>kapt</goal>
                </goals>
                <configuration>
                    <sourceDirs>
                        <sourceDir>src/main/kotlin</sourceDir>
                        <sourceDir>src/main/java</sourceDir>
                    </sourceDirs>
                    <annotationProcessorPaths>
                        <!-- Specify your annotation processors here. -->
                        <annotationProcessorPath>
                            <groupId>org.springframework.boot</groupId>
                            <artifactId>spring-boot-configuration-processor</artifactId>
                            <version>${spring.boot.version}</version>
                        </annotationProcessorPath>
                    </annotationProcessorPaths>
                </configuration>
            </execution>
            <execution>
                <id>compile</id>
                <phase>compile</phase>
                <goals>
                    <goal>compile</goal>
                </goals>
            </execution>
            <execution>
                <id>test-compile</id>
                <phase>test-compile</phase>
                <goals>
                    <goal>test-compile</goal>
                </goals>
            </execution>
        </executions>
        <dependencies>
            <dependency>
                <groupId>org.jetbrains.kotlin</groupId>
                <artifactId>kotlin-maven-allopen</artifactId>
                <version>1.2.20</version>
            </dependency>
        </dependencies>
    </plugin>
</plugins>
```



## 二、使用插件生成

我之前也是使用了同样的插件,但是始终生成不出来文件,直到看了kotlin官方文档我才发现有这么一句话

![img](https://oscimg.oschina.net/oscnet/32fe5bfc75b9a2618f9da47c91e0873bb16.jpg)

文字的意思是: 

**"请注意，kapt仍然不支持IntelliJ IDEA自己的构建系统。当你想要重新运行注释处理器时，可以从“Maven Projects”工具栏启动构建。"**

**很是坑爹啊,你也不标红也不加粗是想怎样啊**

好了,那就按照他说的做吧, 双击下面的插件按钮就可以生产`spring-configuration-metadata.json`文件了

![img](https://oscimg.oschina.net/oscnet/3622930814489ff201bd0db85aa89873930.jpg)

**参考文档:**

> <https://stackoverflow.com/questions/37858833/spring-configuration-metadata-json-file-is-not-generated-in-intellij-idea-for-ko>
>
> <https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-kotlin.html#boot-features-kotlin-configuration-properties>
>
> <https://kotlinlang.org/docs/reference/kapt.html
